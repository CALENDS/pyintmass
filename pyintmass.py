import matplotlib.pyplot as plt
import numpy as np
import astropy.cosmology as co
import astropy.units as uu
import astropy
import sys
from astropy.coordinates.angle_utilities import angular_separation
from scipy import interpolate

def compute_profile(im,zl,RAcen,DECcen,step,rmax=None,Ell=0.0,Posang=0.0,mode='kappa'):
        cd = co.FlatLambdaCDM(H0=70*uu.km/(uu.megaparsec * uu.s), Om0=0.3, Tcmb0 = 2.725*uu.K, Neff=3.05, m_nu=[0., 0., 0.06]*uu.eV, Ob0 = 0.0483)
        kpc_per_arcsec=1.0/cd.arcsec_per_kpc_proper(zl)
        dol=cd.angular_diameter_distance(zl)

        #in 10^12 Msol/kpc2
        sigcrit=(astropy.constants.c**2.0/astropy.constants.G/dol/4.0/np.pi).to(1e12*uu.solMass/uu.kpc/uu.kpc)
        
        #get_step:
        #cd=np.dot(np.diag(self.wcs.wcs.get_cdelt()), self.wcs.wcs.get_pc()) #from pywcs
        #dx = np.sqrt(cd[0, 0]**2 + cd[1, 0]**2)
        #dy = np.sqrt(cd[0, 1]**2 + cd[1, 1]**2)
        #steps = dy*3600.0*uu.arcsec/uu.pix

        imscale=im.wcs.get_step()[0]*3600.0*uu.arcsec/uu.pix #taken from im
        pixarea=(imscale*kpc_per_arcsec)**2.0
        
        integral=[]
        rad=[]
        ratio=np.sqrt((1.0-Ell)/(1.0+Ell))

        if rmax==None:
           [DECmin,RAmin,DECmax,RAmax]=im.wcs.get_range()
           r1=angular_separation(RAmin,DECmin,RAcen,DECcen)*3600.0
           r2=angular_separation(RAmax,DECmin,RAcen,DECcen)*3600.0
           r3=angular_separation(RAmax,DECmax,RAcen,DECcen)*3600.0
           r4=angular_separation(RAmin,DECmax,RAcen,DECcen)*3600.0
           rmax=np.max([r1,r2,r3,r4])
        else:
           rmax/=kpc_per_arcsec.value

        step/=kpc_per_arcsec.value

        rad_vect=np.arange(0.0,rmax,step)
        integral_value=0.0

        cospa = np.cos(np.radians(Posang))
        sinpa = np.sin(np.radians(Posang))

        center = im.wcs.sky2pix(np.array((DECcen,RAcen)), unit=uu.deg)[0]

        x, y = np.meshgrid((np.arange(im.shape[1]) - center[1]) * imscale.value,
                           (np.arange(im.shape[0]) - center[0]) * imscale.value)


        distell = np.sqrt((((x * cospa + y * sinpa) / step ) ** 2 +
                ((y * cospa - x * sinpa) / (step*ratio)) ** 2))



        start=1
#for loop is to be optimised (with an histogram ?
        for r in rad_vect:
                sys.stdout.write("r=%.2f / %.2f\r"%(r,rmax))
                sys.stdout.flush()
                rb=r*ratio
                rbp=(r+step)*ratio

                #im.mask_ellipse((DECcen,RAcen),(r+step,rbp), Posang, inside=False)
                #im.mask_ellipse((DECcen,RAcen),(r,rb), Posang, inside=True)

                kselval=np.where((distell>=r/step)&(distell<(r/step+1.0)))
                kselval1=np.where((distell>=r/step)&(distell<(r/step+0.5)))
                kselval2=np.where((distell>=r/step+0.5)&(distell<(r/step+1.0)))
                density=np.mean(im._data[kselval])
                density1=np.mean(im._data[kselval1])
                density2=np.mean(im._data[kselval2])
                rad.append(density)
                area=np.pi*((r+step)*rbp-r*rb)*(kpc_per_arcsec.value**2.0)
                area1=np.pi*((r+step/2.0)*(r+step/2.0)*ratio-r*rb)*(kpc_per_arcsec.value**2.0)

                area2=np.pi*((r+step)*rbp-(r+step/2.0)*(r+step/2.0)*ratio)*(kpc_per_arcsec.value**2.0)

                integral_value+=density1*area1
                integral.append(integral_value)
                integral_value+=density2*area2

        sys.stdout.write("\n")

        if(mode=='kappa'):  #converts kappa into Sigma / Mtot
                rad=np.array(rad)*sigcrit 
                integral=np.array(integral)*sigcrit*uu.kpc*uu.kpc
        if(mode=='mpix2'):  
                rad=np.array(rad)/pixarea
                integral=np.array(integral)/pixarea
        if(mode=='mkpc2'):  #already in correct units
                rad=np.array(rad)
                integral=np.array(integral)

        rad_vect+=step/2.0
        rad_vect*=kpc_per_arcsec.value

        #Output always in 10^12 Msun/kpc2 (radial) and 10^12 Msun (integral)
        return rad_vect,rad,integral

def reinstein(im,zl,zs,RAcen,DECcen):
        cd = co.FlatLambdaCDM(H0=70*uu.km/(uu.megaparsec * uu.s), Om0=0.3, Tcmb0 = 2.725*uu.K, Neff=3.05, m_nu=[0., 0., 0.06]*uu.eV, Ob0 = 0.0483)

        pixscale=im.wcs.get_step()[0]*3600.0 #taken from im
        step=pixscale*10.0
        
        integral=[]
        rad=[]

        [DECmin,RAmin,DECmax,RAmax]=im.wcs.get_range()
        r1=angular_separation(RAmin,DECmin,RAcen,DECcen)*3600.0
        r2=angular_separation(RAmax,DECmin,RAcen,DECcen)*3600.0
        r3=angular_separation(RAmax,DECmax,RAcen,DECcen)*3600.0
        r4=angular_separation(RAmin,DECmax,RAcen,DECcen)*3600.0
        print(r1,r2,r3,r4)
        rmax=np.max([r1,r2,r3,r4])

        rad_vect=np.arange(0.0,rmax,step)
        integral_value=0.0

        center = im.wcs.sky2pix(np.array((DECcen,RAcen)), unit=uu.deg)[0]

        x, y = np.meshgrid((np.arange(im.shape[1]) - center[1]) * pixscale,
                           (np.arange(im.shape[0]) - center[0]) * pixscale)

        dist = np.sqrt(x**2+y**2)/step

#for loop is to be optimised (with an histogram ?
        for r in rad_vect:
                rb=r
                rbp=r+step

                kselval=np.where((dist>=r/step)&(dist<(r/step+1.0)))
                kselval1=np.where((distell>=r/step)&(distell<(r/step+0.5)))
                kselval2=np.where((distell>=r/step+0.5)&(distell<(r/step+1.0)))
                if(len(kselval[0])>0):
                  density=np.mean(im._data[kselval])
                  density1=np.mean(im._data[kselval1])
                  density2=np.mean(im._data[kselval2])
                  sys.stdout.write("r=%.2f / %.2f density %.2f\r"%(r,rmax,density))
                  sys.stdout.flush()
                else:
                  density=0.0
                  density1=0.0
                  density2=0.0

                area=np.pi*(rbp*rbp-r*r)
                area1=np.pi*((r+step/2.0)*(r+step/2.0)-r*r)
                area2=np.pi*(rbp*rbp-(r+step/2.0)*(r+step/2.0))
                integral_value+=density1*area1
                integral.append(integral_value/(np.pi*(r+step*0.5)*(r+step*0.5)))
                integral_value+=density2*area2
                

        sys.stdout.write("\n")

        rad_vect+=step/2.0

        integral*=cd.angular_diameter_distance_z1z2(zl,zs)/cd.angular_diameter_distance(zs)

        yreduced = np.array(integral) - 1.0
        freduced = interpolate.UnivariateSpline(rad_vect, yreduced, s=0)
        re=freduced.roots()

        return rad_vect,integral,re
